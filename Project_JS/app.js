﻿var myApp = angular.module("myApp", []);
var elementInfo = new Object();
var question_needs_set = true;
var autoKey = '001';

myApp.controller('mainController', function ($scope) {
    var self = this;

    $scope.atomicNumber = null;
    $scope.atomicWeight = null;
    $scope.atomicSymbol = "";
    $scope.atomicName = "";
    $scope.electronConfig = "";

    $scope.elements = new Object();
    $scope.count = 1;
    $scope.modalAppear = false;


    $scope.buildTable = function (num) {
        if (num > 1 && num < 18 || num > 38 && num < 49 ||
            num > 20 && num < 31 || num == 93 || num == 111 ||
            num > 126 && num < 147 || num > 162 && num < 165 ||
            num == 162 || num == 180) {
            return "no-shadow";
        }
        else if (!(num in $scope.elements)) {
            $scope.elements[num] = $scope.count;
            if (num == 92 || num == 110)
                $scope.count += 15;
            if (num == 126)
                $scope.count = 56;
            if (num == 161)
                $scope.count = 88;
            $scope.count++;
        }
    }

    $scope.elemIdx = function (index) {
        if ($scope.elements[index]) {
            return $scope.elements[index];
        }
        return -1;
    }

    $scope.showModal = function (show, index) {
        var key = null;

        if ($scope.modalAppear)
            show = false;

        $scope.backdrop = "backdropLight";
        $scope.modalAppear = false;

        if (index && show) {
            key = self.indexToKey(index);
            if (key != "" && !(key in elementInfo)) {
                $scope.backdrop = "backdropDim";
                $scope.atomicNumber = index;
                $scope.atomicWeight = elementData[key][2];
                $scope.atomicSymbol = elementData[key][1];
                $scope.atomicName = elementData[key][0];
                $scope.electronConfig = elementData[key][3];
                $scope.modalAppear = show;
            }
        }

    };
    $scope.getElementSymbol = function (index) {
        index = self.indexToKey(index);
        if (index in elementData && !(index in elementInfo))
        {

            var syms = document.getElementsByClassName('symbol');
            for (i = 0; i < syms.length; i++) {
                if (syms[i].innerHTML.trim() != '')
                    syms[i].parentNode.classList.remove('undiscovered');
            }


            return elementData[index][1];

        }
        return "";
    }

    self.buildGrid = function () {
        $scope.grid = []
        for (var i = 1; i <= 180; i++) {
            $scope.grid.push(i);
        }
        return $scope.grid;
    }();

    self.indexToKey = function (index) {
        if (index === -1) {
            index = "";
        } else if (index < 10) {
            index = '00' + index;
        } else if (index < 100) {
            index = '0' + index;
        }
        return index;
    };
});

myApp.controller('listController', function ($scope) {
    var self = this;
    self.answer = null;
    self.key = null;
    self.elementClicked = null;
    //self.elementInfo = elementData;
    self.indexToKey = function (index) {
        if (index === -1) {
            index = "";
        } else if (index < 10) {
            index = '00' + index;
        } else if (index < 100) {
            index = '0' + index;
        }
        return index;
    };

    {
        for (key in Object.keys(elementData))
        {
            if (key == 0)
                continue;
            key = self.indexToKey(key);
            elementInfo[key] = elementData[key];
        }
        elementInfo['118'] = elementData['118'];
    }

    $scope.getKeys = function () {
        return Object.keys(elementInfo).sort();
    }
    $scope.getElements = function () {
        return elementInfo;
    }

    $scope.setSymbolAnswer = function (key) {
        var listitems = document.getElementsByClassName('element-list-item');
        for (i = 0; i < listitems.length; i++) {
            listitems[i].classList.remove("element-question");
            if (listitems[i].innerHTML.trim() == self.getElementByKey(key)[0])
            {
                listitems[i].classList.add("element-question");
                question_needs_set = false;
            }
        }

        $scope.atomicSymbol = "";
        self.elementClicked = self.getElementByKey(key);
        self.key = key;
        self.answer = self.elementClicked[1];

        document.getElementById('input-field').focus();
        var feedback = document.getElementById('feedback-div');
        feedback.innerHTML = '';
        feedback.style.boxShadow = "none";
    }

    $scope.checkCorrectSymbol = function (event) {
        var feedback = document.getElementById('feedback-div');
        if (event.which === 13) {
            $scope.atomicSymbol = $scope.atomicSymbol[0].toUpperCase() + $scope.atomicSymbol.slice(1);
            if (self.answer === $scope.atomicSymbol) {
                delete elementInfo[self.key];
                feedback.innerHTML = '&#x2713';
                feedback.style.color = 'green';
                feedback.style.boxShadow = "0 0 10px green";
                question_needs_set = true;
            } else {
                feedback.innerHTML = '&#x2717';
                feedback.style.color = 'red';
                feedback.style.boxShadow = "0 0 10px red";
            }
            $scope.atomicSymbol = "";

            if (question_needs_set) {
                if (!(autoKey in elementInfo)) {
                    autoKey++;
                    autoKey = self.indexToKey(autoKey);
                }
                $scope.setSymbolAnswer(autoKey);
            }
        }
    }

    self.getElementByKey = function (key) {
        return elementInfo[key];
    };
    $scope.setSymbolAnswer('001');
});

document.addEventListener('DOMContentLoaded', function () {
    document.getElementsByClassName('element-list-item')[0].classList.add("element-question");
});